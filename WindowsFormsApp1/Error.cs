﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    internal class Error
    {
        public string Mensagem { get; set; }
        public DateTime Horario { get; set; }

        public Error(string mensagem, DateTime horario)
        {
            Mensagem = mensagem;
            Horario = horario;
        }
    }
}
