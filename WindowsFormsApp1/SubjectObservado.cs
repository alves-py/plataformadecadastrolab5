﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    internal class SubjectObservado
    {
        private List<IObservador> observadores = new List<IObservador>();

        public void AdicionarObservador(IObservador observador)
        {
            observadores.Add(observador);
        }

        public void NotificarObservadores(string mensagem)
        {
            foreach (var observador in observadores)
            {
                observador.Update(mensagem);
            }
        }
    }
}
