﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.ListView;

namespace WindowsFormsApp1
{
    public partial class Main : Form
    {
        private List<User> user = new List<User>();
        private SubjectObservado subjectObservado = new SubjectObservado();
        private Stack<Error> erros = new Stack<Error>();
        public Main()
        {
            InitializeComponent();
            IniciarView();
            AdicionarObservador(new Observador());
        }


        private void Register_Click(object sender, EventArgs e)
        {
            try
            {
                var name = NameText.Text;
                var lastName = LastNameText.Text;
                var email = EmailText.Text.ToLower();

                if (string.IsNullOrWhiteSpace(name))
                {
                    throw new Exception("Por favor insira seu Nome");
                }
                if (string.IsNullOrWhiteSpace(lastName))
                {
                    throw new Exception("Por favor insira seu sobrenome.");
                }
                if (string.IsNullOrWhiteSpace(email))
                {
                    throw new Exception("Por favor insira seu e-mail");
                }

                User newUser = new User(name, lastName, email);
                user.Add(newUser);

                CounterUser();
                AttListView();
                subjectObservado.NotificarObservadores($"Usuário: {name} adicionado com sucesso!");
            } catch (Exception ex)
            {
                erros.Push(new Error(ex.Message, DateTime.Now));
                subjectObservado.NotificarObservadores("Error: " + ex.Message);
            }

        }

        private void Search_Click(object sender, EventArgs e)
        {
            listView1.Items.Clear();
            var usuariosFiltrados = user.Where(u => u.Email.ToLower().Contains(textSearchEmail.Text.ToLower())).ToList();
            foreach (var usuario in usuariosFiltrados)
            {
                ListViewItem item = new ListViewItem(new[] { usuario.Name, usuario.LastName, usuario.Email });
                listView1.Items.Add(item);
                listView1.Focus();
                item.Selected = true;
                break;
            }
        }
        private void IniciarView()
        {
            listView1.Columns.Add("Nome", 200, HorizontalAlignment.Center);
            listView1.Columns.Add("Sobrenome", 200, HorizontalAlignment.Center);
            listView1.Columns.Add("Email", 250, HorizontalAlignment.Center);

            listView1.View = View.Details;

            listView1.FullRowSelect = true;
            listView1.GridLines = true;
        }

        private void AttListView()
        {
            listView1.Items.Clear();

            foreach (User user in user)
            {
                ListViewItem itemNew = new ListViewItem(new[] { user.Name, user.LastName, user.Email });
                listView1.Items.Add(itemNew);
            }

        }

        private void CounterUser()
        {
            textCountUser.Text = $"Total de usuários: {user.Count}";
        }

        private void growingButton_Click(object sender, EventArgs e)
        {
            listView1.Items.Clear();
            var contatosFiltrados = user.OrderBy(c => c.Name).ToList();
            foreach (var contatos in contatosFiltrados)
            {
                ListViewItem item = new ListViewItem(new[] { contatos.Name, contatos.LastName, contatos.Email });
                listView1.Items.Add(item);
            }
        }

        private void descendingButton_Click(object sender, EventArgs e)
        {
            listView1.Items.Clear();
            var contatosFiltrados = user.OrderByDescending(c => c.Name).ToList();
            foreach (var contatos in contatosFiltrados)
            {
                ListViewItem item = new ListViewItem(new[] { contatos.Name, contatos.LastName, contatos.Email });
                listView1.Items.Add(item);
            }
        }

        public void AdicionarObservador(IObservador observador)
        {
            subjectObservado.AdicionarObservador(observador);
        }

        private void HistoryButton_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            foreach (Error erro in erros)
            {
                sb.AppendLine($"[{erro.Horario}] {erro.Mensagem}");
            }
            MessageBox.Show(sb.ToString(), "Histórico de erros: ", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void clearButton_Click(object sender, EventArgs e)
        {
            NameText.Clear();
            LastNameText.Clear();
            EmailText.Clear();

            subjectObservado.NotificarObservadores("Campos limpos! :D ");
        }
    }
}
