﻿namespace WindowsFormsApp1
{
    partial class Main
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.RegisterButton = new System.Windows.Forms.Button();
            this.labelNome = new System.Windows.Forms.Label();
            this.NameText = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.LastNameText = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.EmailText = new System.Windows.Forms.TextBox();
            this.SearchButton = new System.Windows.Forms.Button();
            this.textSearchEmail = new System.Windows.Forms.TextBox();
            this.labelSearch = new System.Windows.Forms.Label();
            this.GrowingButton = new System.Windows.Forms.Button();
            this.DescendingButton = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.listView1 = new System.Windows.Forms.ListView();
            this.textCountUser = new System.Windows.Forms.TextBox();
            this.HistoryButton = new System.Windows.Forms.Button();
            this.clearButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // RegisterButton
            // 
            this.RegisterButton.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.RegisterButton.Location = new System.Drawing.Point(38, 245);
            this.RegisterButton.Name = "RegisterButton";
            this.RegisterButton.Size = new System.Drawing.Size(134, 33);
            this.RegisterButton.TabIndex = 6;
            this.RegisterButton.Text = "Register";
            this.RegisterButton.UseVisualStyleBackColor = true;
            this.RegisterButton.Click += new System.EventHandler(this.Register_Click);
            // 
            // labelNome
            // 
            this.labelNome.AutoSize = true;
            this.labelNome.Location = new System.Drawing.Point(166, 42);
            this.labelNome.Name = "labelNome";
            this.labelNome.Size = new System.Drawing.Size(44, 16);
            this.labelNome.TabIndex = 0;
            this.labelNome.Text = "Nome";
            // 
            // NameText
            // 
            this.NameText.BackColor = System.Drawing.Color.AliceBlue;
            this.NameText.Location = new System.Drawing.Point(38, 65);
            this.NameText.Name = "NameText";
            this.NameText.Size = new System.Drawing.Size(293, 22);
            this.NameText.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(149, 108);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 16);
            this.label1.TabIndex = 2;
            this.label1.Text = "Sobrenome";
            // 
            // LastNameText
            // 
            this.LastNameText.BackColor = System.Drawing.Color.AliceBlue;
            this.LastNameText.Location = new System.Drawing.Point(38, 130);
            this.LastNameText.Name = "LastNameText";
            this.LastNameText.Size = new System.Drawing.Size(293, 22);
            this.LastNameText.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(161, 170);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 16);
            this.label2.TabIndex = 4;
            this.label2.Text = "E-mail";
            // 
            // EmailText
            // 
            this.EmailText.BackColor = System.Drawing.Color.AliceBlue;
            this.EmailText.Location = new System.Drawing.Point(38, 194);
            this.EmailText.Name = "EmailText";
            this.EmailText.Size = new System.Drawing.Size(293, 22);
            this.EmailText.TabIndex = 5;
            // 
            // SearchButton
            // 
            this.SearchButton.Location = new System.Drawing.Point(619, 394);
            this.SearchButton.Name = "SearchButton";
            this.SearchButton.Size = new System.Drawing.Size(138, 28);
            this.SearchButton.TabIndex = 14;
            this.SearchButton.Text = "Buscar";
            this.SearchButton.UseVisualStyleBackColor = true;
            this.SearchButton.Click += new System.EventHandler(this.Search_Click);
            // 
            // textSearchEmail
            // 
            this.textSearchEmail.BackColor = System.Drawing.Color.AliceBlue;
            this.textSearchEmail.Location = new System.Drawing.Point(546, 366);
            this.textSearchEmail.Name = "textSearchEmail";
            this.textSearchEmail.Size = new System.Drawing.Size(265, 22);
            this.textSearchEmail.TabIndex = 13;
            // 
            // labelSearch
            // 
            this.labelSearch.AutoSize = true;
            this.labelSearch.Location = new System.Drawing.Point(626, 347);
            this.labelSearch.Name = "labelSearch";
            this.labelSearch.Size = new System.Drawing.Size(113, 16);
            this.labelSearch.TabIndex = 12;
            this.labelSearch.Text = "Buscar por E-mail";
            // 
            // GrowingButton
            // 
            this.GrowingButton.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.GrowingButton.Location = new System.Drawing.Point(370, 284);
            this.GrowingButton.Name = "GrowingButton";
            this.GrowingButton.Size = new System.Drawing.Size(272, 37);
            this.GrowingButton.TabIndex = 10;
            this.GrowingButton.Text = "Ordenar em ordem crescente";
            this.GrowingButton.UseVisualStyleBackColor = true;
            this.GrowingButton.Click += new System.EventHandler(this.growingButton_Click);
            // 
            // DescendingButton
            // 
            this.DescendingButton.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.DescendingButton.Location = new System.Drawing.Point(717, 284);
            this.DescendingButton.Name = "DescendingButton";
            this.DescendingButton.Size = new System.Drawing.Size(272, 37);
            this.DescendingButton.TabIndex = 11;
            this.DescendingButton.Text = "Ordenar em ordem decrescente";
            this.DescendingButton.UseVisualStyleBackColor = true;
            this.DescendingButton.Click += new System.EventHandler(this.descendingButton_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(84, 394);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(0, 16);
            this.label3.TabIndex = 17;
            // 
            // listView1
            // 
            this.listView1.BackColor = System.Drawing.Color.AliceBlue;
            this.listView1.HideSelection = false;
            this.listView1.Location = new System.Drawing.Point(370, 42);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(619, 215);
            this.listView1.TabIndex = 8;
            this.listView1.UseCompatibleStateImageBehavior = false;
            // 
            // textCountUser
            // 
            this.textCountUser.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textCountUser.Location = new System.Drawing.Point(370, 263);
            this.textCountUser.Name = "textCountUser";
            this.textCountUser.ReadOnly = true;
            this.textCountUser.Size = new System.Drawing.Size(293, 15);
            this.textCountUser.TabIndex = 0;
            // 
            // HistoryButton
            // 
            this.HistoryButton.Location = new System.Drawing.Point(875, 413);
            this.HistoryButton.Name = "HistoryButton";
            this.HistoryButton.Size = new System.Drawing.Size(138, 28);
            this.HistoryButton.TabIndex = 15;
            this.HistoryButton.Text = "Histórico de erros";
            this.HistoryButton.UseVisualStyleBackColor = true;
            this.HistoryButton.Click += new System.EventHandler(this.HistoryButton_Click);
            // 
            // clearButton
            // 
            this.clearButton.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.clearButton.Location = new System.Drawing.Point(197, 245);
            this.clearButton.Name = "clearButton";
            this.clearButton.Size = new System.Drawing.Size(134, 33);
            this.clearButton.TabIndex = 7;
            this.clearButton.Text = "Limpar";
            this.clearButton.UseVisualStyleBackColor = true;
            this.clearButton.Click += new System.EventHandler(this.clearButton_Click);
            // 
            // Main
            // 
            this.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.ClientSize = new System.Drawing.Size(1016, 457);
            this.Controls.Add(this.clearButton);
            this.Controls.Add(this.HistoryButton);
            this.Controls.Add(this.textCountUser);
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.DescendingButton);
            this.Controls.Add(this.GrowingButton);
            this.Controls.Add(this.labelSearch);
            this.Controls.Add(this.textSearchEmail);
            this.Controls.Add(this.SearchButton);
            this.Controls.Add(this.EmailText);
            this.Controls.Add(this.LastNameText);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.NameText);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.labelNome);
            this.Controls.Add(this.RegisterButton);
            this.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Name = "Main";
            this.Text = "Lab 4";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button RegisterButton;
        private System.Windows.Forms.Label labelNome;
        private System.Windows.Forms.TextBox NameText;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox LastNameText;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox EmailText;
        private System.Windows.Forms.Button SearchButton;
        private System.Windows.Forms.TextBox textSearchEmail;
        private System.Windows.Forms.Label labelSearch;
        private System.Windows.Forms.Button GrowingButton;
        private System.Windows.Forms.Button DescendingButton;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.TextBox textCountUser;
        private System.Windows.Forms.Button HistoryButton;
        private System.Windows.Forms.Button clearButton;
    }
}

